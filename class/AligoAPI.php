<?php

namespace App;

use GuzzleHttp\Client;

class AligoAPI
{
  private $key = '75uwgjdktlqlxcs390fqry3qgh3e11gd';
  private $user_id = 'ywkim';
  private $sender = '01022656264';

  /*
   *
   * $data => [
   *  stdClass Object(
   *    [phone] => string
   *    [content] => string
   * ];
   *
   */
  public function send($data)
  {
    $client = new Client();

    $form_params = [
      'testmode_yn' => 'Y', // test mode
      'key' => $this->key,
      'user_id' => $this->user_id,
      'sender' => $this->sender,
      'cnt' => count($data),
      'msg_type' => 'SMS'
    ];

    for ($i = 0; $i < count($data); $i++) {
      $form_params = array_merge($form_params, [
        'rec_'.($i + 1) => $data[$i]->phone,
        'msg_'.($i + 1) => $data[$i]->content
      ]);
    }

    $response = $client->post('https://apis.aligo.in/send_mass/', compact('form_params'));

    $body = json_decode($response->getBody());

    if ($body->result_code < 0) {
      $this->sendException($body->message, $body->result_code);

      return false;
    }

    return true;
  }

  public function sendException($message, $code)
  {
    // error handling...
  }
}
