<?php

namespace App;

class File
{
  public function read($file_path)
  {
    try {
      if (! is_readable($file_path)) {
        throw new \Exception('File not found or not readable.');
      }
    } catch (\Exception $e) {
      die($e->getMessage().PHP_EOL);
    }

    $file = fopen($file_path, 'r');

    return fread($file, filesize($file_path));
  }

  private function regex($content)
  {
    try {
      if (! preg_match_all('/^(([0-9]*),(.*)([\r\n]*))+$/', $content)) {
        throw new \Exception('The file content format is not correct.');
      }

      return true;
    } catch (\Exception $e) {
      die($e->getMessage().PHP_EOL);
    }
  }

  public function formatted($content)
  {
    if ($this->regex($content)) {
      $content_arr = explode(PHP_EOL, $content);

      for ($i = 0; $i < count($content_arr); $i++) {
        if ($content_arr[$i] == null) {
          break;
        }

        list($phone, $content) = explode(',', $content_arr[$i], 2);

        $formatted_content[$i] = new \stdClass();
        $formatted_content[$i]->phone = $phone;
        $formatted_content[$i]->content = $content;
      }

      return $formatted_content;
    }
  }
}
