<?php

require_once __DIR__.'/vendor/autoload.php';

use App\File as File;
use App\AligoAPI as AligoAPI;

$file = new File();

$list = $file->formatted($file->read('list.txt'));

$aligo_api = new AligoAPI();

if ($aligo_api->send($list)) {
  echo '성공'.PHP_EOL;
} else {
  echo '실패'.PHP_EOL;
}

